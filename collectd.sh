#!/bin/sh
echo "*****************************************************";
echo "*";
echo "*   Collectd Script Install by anhnv@hostvn.com";
echo "*      Version: 1.7.2 - Release 28/02/2017";
echo "*         Report if you meet trouble";
echo "*";
echo "*****************************************************";

GRAPHITE_HOSTNAME=eye.hostvn.net
CARBON_PORT=1995
yum install bind-utils epel-release -y

ping -c 1 8.8.8.8 > /dev/null 2>&1
if [ $? != 0 ]; then
	printf "Could not connect to Internet\nExit !\n"
	exit
fi

GRAPHITE_IP=$(host $GRAPHITE_HOSTNAME | awk '/has address/ { print $4 }')

CSFBIN="/usr/sbin/csf"
if [ -f "$CSFBIN" ]; then
 csf -a $GRAPHITE_IP

fi

OS_VER=`cat /etc/redhat-release |cut -d\  -f3`;
  if [ "$OS_VER" = "release" ]; then
          OS_VER=`cat /etc/redhat-release | cut -d\  -f4`
  fi
OS_VER=`echo $OS_VER | cut -d. -f1,1`


if [ $OS_VER -eq "6" ]; then
cat > /etc/yum.repos.d/collectd-ci.repo <<EOF
[collectd-ci]
name=collectd CI
baseurl=http://pkg.ci.collectd.org/rpm/master/epel-6-\$basearch
enabled=1
gpgkey=http://pkg.ci.collectd.org/pubkey.asc
gpgcheck=1
repo_gpgcheck=1
EOF
yum install collectd collectd-disk collectd-mysql -y
else
  yum install collectd collectd-mysql -y
fi

echo "Enter MySQL User (root, da_admin,...): "
read MySQLUSER
echo "Enter MySQL Password: "
read MySQLPASSWROD
echo "Enter Prefix (cloud, customer,asv,...):"
read PREFIXCLLD

cat > /etc/collectd.conf <<EOF
Hostname    "$(hostname)"
Interval     10
Timeout         10
LoadPlugin syslog
<Plugin syslog>
        LogLevel info
</Plugin>
LoadPlugin cpu
LoadPlugin df
LoadPlugin disk
LoadPlugin interface
LoadPlugin load
LoadPlugin memory
LoadPlugin swap
LoadPlugin uptime
LoadPlugin FileCount
LoadPlugin mysql
LoadPlugin write_graphite

<Plugin "disk">
  Disk "/^sd/"
  Disk "/^xvd/"
  IgnoreSelected false
</Plugin>
<Plugin "interface">
  Interface "/^bond/"
  Interface "/^eth/"
  Interface "/^em/"
  Interface "/^ens/"
  Interface "/^eno/"
  IgnoreSelected false
</Plugin>
<Plugin "mysql">
  <Database "mysql">
    User "$MySQLUSER"
    Password "$MySQLPASSWROD"
    Port 3306
  </Database>
</Plugin>
<Plugin "filecount">
  <Directory "/var/spool/exim/input">
    Instance "mail-queue"
  </Directory>
</Plugin>

<Plugin "write_graphite">
 <Carbon>
   Host "$GRAPHITE_HOSTNAME"
   Port "$CARBON_PORT"
   Prefix "$PREFIXCLLD."  
   Protocol "tcp"
   EscapeCharacter "_"
   SeparateInstances false
   StoreRates true
   AlwaysAppendDS false
 </Carbon>
</Plugin>
EOF

if [ $OS_VER -eq "6" ]; then
  /etc/init.d/collectd start
  chkconfig collectd on
else
  systemctl start collectd
  systemctl enable collectd
fi

echo "Collectd Install Successfully !"